const angular = require('angular');
import $ from 'jquery';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';
import './css/index.css';

angular.module('calculator', [])
  .controller('CalcController', function($scope) {
    let entry = '';
    let ans = '';
    let current = '';
    let log = '';
    let decimal = true;
    let reset = '';

    $scope.answer = 0;
    $scope.history = 0;

    // round function if answer includes a decimal
    function round(val) {
      val = val.toString().split('');
      if (val.indexOf('.') !== -1) {
        let valTest = val.slice(val.indexOf('.') + 1, val.length);
        val = val.slice(0, val.indexOf('.') + 1);
        let i = 0;
        while (valTest[i] < 1) {
          i++
        }
        valTest = valTest.join('').slice(0, i + 2);
        if (valTest[valTest.length-1] === '0') {
          valTest = valTest.slice(0, -1);
        }
        return val.join('') + valTest;
      } else {
        return val.join('');
      }
    }

    $scope.calc = function(entryParam) {
      entry = entryParam;

      //reset for log after answer to equation.
      if (reset) {
        if (entry === '/' || entry === '*' || entry === '-' || entry === '+') {
          log = ans;
        } else {
          ans = '';
        }
      }
      reset = false;

      // All clear or Clear Entry
      if (entry === 'ac' || entry === 'ce' && current === 'noChange') {
        ans = '';
        current = '';
        entry = '';
        log = '';
        $scope.answer = 0;
        $scope.history = 0;
        decimal = true;
      } else if (entry === 'ce') {
        $scope.history = log.slice(0, -current.length);
        log = log.slice(0, -current.length);
        ans = ans.slice(0, -current.length);
        current = ans;
        if (log.length === 0 || log === ' ') {
          $scope.history = 0;
        }
        $scope.answer = 0;
        entry = '';
        decimal = true;
      }

      // prevents more than one deciminal in a number
      if (entry === '.' || entry === '0.') {
        if (!decimal) {
          entry = '';
        }
      }

      // prevents improper use of first digit
      if (ans.length === 0 && isNaN(entry) && entry !== '.' || ans.length === 0 && entry === '0') {
        entry = '';
        ans = '';
      }

      // prevents extra operators
      if (current !== 'noChange') {
        if (current === '' && isNaN(entry) && entry !== '.' || isNaN(current) && isNaN(entry) && entry !== '.') {
          entry = '';
        }
      }

      // digit combining
      while (Number(entry) || entry === '0' || current === '.') {
        if (isNaN(current) && entry === '0' && current !== '.') {
          entry = '';
        } else if (isNaN(current) && Number(entry) && current !== '.') {
          current = '';
        }
        if (entry === '.') {
          decimal = false;
        }
        if (current === '0.' && isNaN(entry)) {
          entry = '';
        } else {
          if (current[current.length - 1] === '.') {
            current = current.concat(entry);
          } else {
            current += entry;
          }
          ans += entry;
          $scope.answer = current;
          log += entry;
          $scope.history = log;
          entry = '';
        }
      }

      // Operation list
      if (entry === '.') {
        if (current === '' || isNaN(current[current.length - 1])) {
          current = '0.';
          ans += entry;
          $scope.answer = '0.';
          log += current;
          $scope.history = log;

        } else {
          current = current.concat('.');
          ans = ans.concat('.');
          log = ans;
          $scope.history = ans;
          $scope.answer = current;
        }
        entry = '';
        decimal = false;

      } else if (entry === '/') {
        current = '/';
        ans = round(eval(ans)) + current;
        log += current;
        $scope.history = log;
        $scope.answer = '/';
        entry = '';
        decimal = true;

      } else if (entry === '*') {
        current = '*';
        ans = round(eval(ans)) + current;
        log += 'x';
        $scope.history = log;
        $scope.answer = 'x';
        entry = '';
        decimal = true;

      } else if (entry === '-') {
        current = '-';
        ans = round(eval(ans)) + current;
        log += current;
        $scope.history = log;
        $scope.answer = '-';
        entry = '';
        decimal = true;

      } else if (entry === '+') {
        current = '+';
        ans = round(eval(ans)) + current;
        log += current;
        $scope.history = log;
        $scope.answer = '+';
        entry = '';
        decimal = true;

      } else if (entry === '=') {
        if (current[current.length - 1] === '.') {
          entry = '';
        } else {
          current = eval(ans).toString();
          $scope.answer = round(eval(ans));
          ans = round(eval(ans));
          log += entry + ans;
          $scope.history = log;
          log = ans;
          entry = '';
          reset = true;
          decimal = true;
        }
        current = 'noChange';
      }
      entry = '';

      if (reset) {
        log = '';
      }

      // max digits on screen
      if ($scope.answer.length > 16 || $scope.history.length > 16) {
        $scope.answer = '0';
        $scope.history = 'Digit Limit Met';
        current = '';
        ans = '';
        log = '';
        decimal = true;
      }

    };

  });
